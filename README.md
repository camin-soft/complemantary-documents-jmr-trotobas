# Complementary documents for Upper-Limb Exoskeletons, Orthotics and Prosthetics featured in the ASME Journal of Mechanisms and Robotics

## It includes:
* a folder containing example of graphs
* a quick instructional video explaining how to use the Jupyter Notebook
* the spreadsheet containing the data
* the Jupyter Notebook to filter and visualize data
* the tree diagramm representing the categorization

## To install Jupyter Notebook:
Install [Jupyter](https://jupyter.org/install) or install [Anaconda](https://www.anaconda.com/products/individual)

## To run the Jupyter Notebook:
* Run Jupyter, it can open in your browser. 
* Navigate through your directories to open the jupyter notebook:  Trotobas_data_filter_visualization_JMR.ipynb
* Make sure that the spreadsheet is in the same folder or you have to change the path directory in the first cell of the jupyter notebook.
